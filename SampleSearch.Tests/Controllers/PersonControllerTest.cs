﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonControllerTest.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the Person Controller Test type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace SampleSearch.Tests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web.Http.Results;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Models;
    using SampleSearch.Controllers.Api;

    /// <summary>
    /// Person controller test
    /// </summary>
    [TestClass]
    public class PersonControllerTest
    {
        /// <summary>
        /// Test the Search method of this instance.
        /// </summary>
        [TestMethod]
        public void SearchShouldSucceed()
        {
            const string mockSearchString = "a";
            var controller = new PersonController();
            var result = controller.Search(mockSearchString) as OkNegotiatedContentResult<List<PersonView>>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
        }

        /// <summary>
        /// Test the Search method of this instance with a non-matching search string.
        /// </summary>
        [TestMethod]
        public void SearchReturnsNothingWithUnMatchingSearchString()
        {
            const string mockSearchString = "abcdefghijklmnopqrstuvwzyz";
            var controller = new PersonController();
            var result = controller.Search(mockSearchString) as OkNegotiatedContentResult<List<PersonView>>;
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Content.Count == 0);
        }

        /// <summary>
        /// Test that the Search method using a missing parameter throws an exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "searchText")]
        public void SearchMissingParameterShouldThrowException()
        {
            const string mockSearchString = "";
            var controller = new PersonController();
            controller.Search(mockSearchString);
        }

        /// <summary>
        /// Removes all people.
        /// </summary>
        [TestMethod]
        public void RemoveAllPeopleShouldSucceed()
        {
            var controller = new PersonController();
            var result = controller.RemoveAllPeople() as OkNegotiatedContentResult<int>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.IsTrue(result.Content >= 0);
        }

        /// <summary>
        /// Seeds the database.
        /// </summary>
        [TestMethod]
        public void SeedDatabaseShouldSucceed()
        {
            const int seedUserCount = 100;
            const bool clearData = false;
            ConfigurationManager.AppSettings["SeedUserCount"] = "100";
            ConfigurationManager.AppSettings["SeedUserCountLimit"] = "5000";
            ConfigurationManager.AppSettings["SeedUserUrl"] = "https://api.randomuser.me/?results=";
            var controller = new PersonController();
            var result = controller.SeedDatabase(seedUserCount, clearData) as OkNegotiatedContentResult<int>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.IsTrue(result.Content >= 0);
        }

        /// <summary>
        /// Seeds the database with true clear data should succeed.
        /// </summary>
        [TestMethod]
        public void SeedDatabaseWithTrueClearDataShouldSucceed()
        {
            const int seedUserCount = 100;
            const bool clearData = true;
            ConfigurationManager.AppSettings["SeedUserCount"] = "100";
            ConfigurationManager.AppSettings["SeedUserCountLimit"] = "5000";
            ConfigurationManager.AppSettings["SeedUserUrl"] = "https://api.randomuser.me/?results=";
            var controller = new PersonController();
            var result = controller.SeedDatabase(seedUserCount, clearData) as OkNegotiatedContentResult<int>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.IsTrue(result.Content >= 0);
        }

        /// <summary>
        /// Seeds the database with invalid seed count parameter should fail.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "seedUserCount")]
        public void SeedDatabaseWithInvalidSeedUserCountParameterShouldFail()
        {
            const int seedUserCount = 0;
            const bool clearData = false;
            var controller = new PersonController();
            controller.SeedDatabase(seedUserCount, clearData);
        }
    }
}
