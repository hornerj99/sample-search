﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeControllerTest.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the HomeControllerTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace SampleSearch.Tests.Controllers
{
    using System.Web.Mvc;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using SampleSearch.Controllers;

    /// <summary>
    /// Home controller test
    /// </summary>
    [TestClass]
    public class HomeControllerTest
    {
        /// <summary>
        /// Test the Index method of this instance.
        /// </summary>
        [TestMethod]
        public void Index()
        {
            var controller = new HomeController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test the Search method of this instance.
        /// </summary>
        [TestMethod]
        public void Search()
        {
            var controller = new HomeController();
            var result = controller.Search() as ViewResult;
            Assert.IsNotNull(result);
        }
    }
}
