# People Search

This is a sample site that allows a user to perform a search for people’s names.

### Notes

This landing page was built using jQuery and the search page was built using jQuery and React. I hadn’t used React before, so this was a good excuse to work with it. In a real-world scenario, Angular or a jQuery could certainly be used as well.

### Business Requirements

*   Accept search input in a text box and then display a pleasing list of people with matching first or last name.
*   Display at least name, address, username, phone number, and a picture.
*   Seed user data
*   Tools to manually create and delete people
*   Handle slow searches gracefully.
*   Results as use type
*   Multiple views for search results
*   Responsive design

### Technical Requirements

*   An ASP.NET MVC Application.
*   Use Entity Framework Code First.
*   Use Ajax to respond to search request (no full page refresh) using JSON for both the request and the response.
*   Unit Tests for appropriate parts of the application.