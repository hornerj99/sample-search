﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonInitializer.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the database context for people.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace SampleSearch.DAL
{
    using System.Data.Entity;
    using Services;

    /// <inheritdoc />
    /// <summary>
    ///  Create database when needed and load test data into the new database.
    /// </summary>
    public class PersonInitializer : DropCreateDatabaseIfModelChanges<PersonContext>
    {
        /// <inheritdoc />
        /// <summary>
        /// Seed data
        /// </summary>
        /// <param name="context">The context to seed.</param>
        protected override void Seed(PersonContext context)
        {
            var unused = new PersonService();
            PersonService.SeedDatabase();
        }
    }
}
