﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BundleConfig.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the Bundle Configuration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace SampleSearch
{
    using System.Web.Optimization;

    /// <summary>
    /// The configuration for client-side bundles.
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            // TODO: Probably use gulp/webpack instead.
            bundles.Add(new ScriptBundle("~/bundles/main/top/js").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/main/js").Include(
                "~/Scripts/umd/popper.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery.dataTables.js",
                "~/Scripts/dataTables.bootstrap.js",
                "~/Scripts/dataTables.responsive.js",
                "~/Scripts/react/react.js",
                "~/Scripts/react/react-dom.js",
                "~/Scripts/Custom/main.js"));

            bundles.Add(new StyleBundle("~/maincss").Include(
                "~/Content/bootstrap-yeti.css",
                "~/Content/font-awesome.css",
                "~/Content/dataTables.bootstrap.css",
                "~/Content/responsive.bootstrap.css",
                "~/Content/Custom/main.css",
                "~/Content/Custom/search.css"));

            bundles.Add(new StyleBundle("~/bundles/search/css").Include(
                "~/components/search/app.css",
                "~/components/search/list/app.css",
                "~/components/search/tiles/app.css"));
        }
    }
}
