﻿// <copyright file="PersonController.cs" company="SampleSearch">
// Copyright (c) SampleSearch. All rights reserved.
// </copyright>

namespace SampleSearch.Controllers.Api
{
    using System;
    using System.Web.Http;
    using Services;

    /// <summary>
    /// API controller for a Person
    /// </summary>
    /// <seealso cref="ApiController" />
    [RoutePrefix("api/person")]
    public class PersonController : ApiController
    {
        /// <summary>
        /// Searches the specified search text.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <returns>OK HTTP Result if successful and a list of matching people.</returns>
        [Route("search")]
        [HttpPost]
        public IHttpActionResult Search([FromBody]string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                throw new ArgumentNullException(nameof(searchText));
            }

            var matchingPeople = PersonService.FindPeople(searchText);
            return this.Ok(matchingPeople);
        }

        /// <summary>
        /// Provides an endpoint to allow the user to truncate the people database.
        /// </summary>
        /// <returns>Total number of people in the database.</returns>
        [Route("seed/clear")]
        [HttpGet]
        public IHttpActionResult RemoveAllPeople()
        {
            PersonService.ClearDatabase();
            return this.Ok(PersonService.TotalPeople());
        }

        /// <summary>
        /// Provides an endpoint to allow the user to manually seed the database.
        /// </summary>
        /// <param name="seedUserCount">The seed user count.</param>
        /// <param name="clearData">if set to <c>true</c> [clear data].</param>
        /// <returns>Total number of people in the database.</returns>
        [Route("seed/{seedUserCount:int}/{clearData}")]
        [HttpGet]
        public IHttpActionResult SeedDatabase(int seedUserCount, bool clearData)
        {
            if (seedUserCount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(seedUserCount));
            }

            PersonService.SeedDatabase(seedUserCount, clearData);
            return this.Ok(PersonService.TotalPeople());
        }
    }
}