﻿// <copyright file="HomeController.cs" company="SampleSearch">
// Copyright (c) SampleSearch. All rights reserved.
// </copyright>

namespace SampleSearch.Controllers
{
    using System.Web.Mvc;
    using Services;

    /// <summary>
    /// The Home controller.
    /// </summary>
    /// <seealso cref="Controller" />
    public class HomeController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>The view.</returns>
        public ActionResult Index()
        {
            this.ViewBag.PeopleCount = PersonService.TotalPeople();
            return this.View();
        }

        /// <summary>
        /// Searches this instance.
        /// </summary>
        /// <returns>The view fo the search page.</returns>
        public ActionResult Search()
        {
            return this.View();
        }
    }
}