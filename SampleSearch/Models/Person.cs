﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Person.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the Person Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace SampleSearch.Models
{
    using System;

    /// <summary>
    /// The model representing a Person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the identifier.  As a primary key, it should be uppercase for EF.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>
        /// The street.
        /// </value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the password salt string.
        /// </summary>
        /// <value>
        /// The password salt.
        /// </value>
        public string Salt { get; set; }

        /// <summary>
        /// Gets or sets the MD5 string.
        /// </summary>
        /// <value>
        /// The MD5 string.
        /// </value>
        public string Md5 { get; set; }

        /// <summary>
        /// Gets or sets the SHA1 string.
        /// </summary>
        /// <value>
        /// The SHA1 string.
        /// </value>
        public string Sha1 { get; set; }

        /// <summary>
        /// Gets or sets the SHA256 string.
        /// </summary>
        /// <value>
        /// The SHA256 string.
        /// </value>
        public string Sha256 { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>
        /// The dob.
        /// </value>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Gets or sets the registration date.
        /// </summary>
        /// <value>
        /// The registration date.
        /// </value>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the identifier.
        /// </summary>
        /// <value>
        /// The name of the identifier.
        /// </value>
        public string IdName { get; set; }

        /// <summary>
        /// Gets or sets the identifier value.
        /// </summary>
        /// <value>
        /// The identifier value.
        /// </value>
        public string IdValue { get; set; }

        /// <summary>
        /// Gets or sets the  phone number.
        /// </summary>
        /// <value>
        /// The  phone number.
        /// </value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the cell phone number.
        /// </summary>
        /// <value>
        /// The cell phone number.
        /// </value>
        public string Cell { get; set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        /// <value>
        /// The nationality.
        /// </value>
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the large picture.
        /// </summary>
        /// <value>
        /// The large picture.
        /// </value>
        public string PictureLarge { get; set; }

        /// <summary>
        /// Gets or sets the medium picture.
        /// </summary>
        /// <value>
        /// The medium picture.
        /// </value>
        public string PictureMedium { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail.
        /// </summary>
        /// <value>
        /// The thumbnail.
        /// </value>
        public string PictureThumbnail { get; set; }
    }
}
