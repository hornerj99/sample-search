﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PersonView.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the Person View Model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace SampleSearch.Models
{
    using System;

    /// <summary>
    /// The model representing a view for the Person object.
    /// </summary>
    public class PersonView
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>
        /// The street.
        /// </value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>
        /// The dob.
        /// </value>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Gets or sets the registration date.
        /// </summary>
        /// <value>
        /// The registration date.
        /// </value>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Gets or sets the  phone number.
        /// </summary>
        /// <value>
        /// The  phone number.
        /// </value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the cell phone number.
        /// </summary>
        /// <value>
        /// The cell phone number.
        /// </value>
        public string Cell { get; set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        /// <value>
        /// The nationality.
        /// </value>
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the large picture.
        /// </summary>
        /// <value>
        /// The large picture.
        /// </value>
        public string PictureLarge { get; set; }

        /// <summary>
        /// Gets or sets the medium picture.
        /// </summary>
        /// <value>
        /// The medium picture.
        /// </value>
        public string PictureMedium { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail picture.
        /// </summary>
        /// <value>
        /// The thumbnail picture.
        /// </value>
        public string PictureThumb { get; set; }
    }
}
