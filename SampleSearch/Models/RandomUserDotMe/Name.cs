// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Name.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   The model representing the a Person's Name returned from the randomuser.me site.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SampleSearch.Models.RandomUserDotMe
{
    /// <summary>
    /// The name.
    /// </summary>
    public class Name
    {
        /// <summary>
        /// Gets or sets the first.
        /// </summary>
        public string First { get; set; }

        /// <summary>
        /// Gets or sets the last.
        /// </summary>
        public string Last { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }
    }
}