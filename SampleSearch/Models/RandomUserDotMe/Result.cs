// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Result.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   The model representing the result object returned from the randomuser.me site.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SampleSearch.Models.RandomUserDotMe
{
    using System;

    /// <summary>
    /// The result.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Gets or sets the cell.
        /// </summary>
        public string Cell { get; set; }

        /// <summary>
        /// Gets or sets the dob.
        /// </summary>
        public DateTime Dob { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Id Id { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        /// Gets or sets the login.
        /// </summary>
        public Login Login { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public Name Name { get; set; }

        /// <summary>
        /// Gets or sets the nat.
        /// </summary>
        public string Nat { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        public Picture Picture { get; set; }

        /// <summary>
        /// Gets or sets the registered.
        /// </summary>
        public DateTime Registered { get; set; }
    }
}