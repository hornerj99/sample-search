// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Location.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   The model representing the a Person's Location returned from the randomuser.me site.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SampleSearch.Models.RandomUserDotMe
{
    /// <summary>
    /// The location.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the postcode.
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        public string Street { get; set; }
    }
}