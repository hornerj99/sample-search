﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeedPerson.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   The model representing a Person returned from the site.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SampleSearch.Models.RandomUserDotMe
{
    using System.Collections.Generic;

    /// <summary>
    ///     The model representing a Person returned from the site.
    /// </summary>
    public class SeedPerson
    {
        /// <summary>
        /// Gets or sets the info.
        /// </summary>
        public Info Info { get; set; }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        public List<Result> Results { get; set; }
    }
}