// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Info.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   The model representing the a Person's Info returned from the randomuser.me site.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SampleSearch.Models.RandomUserDotMe
{
    /// <summary>
    /// The info.
    /// </summary>
    public class Info
    {
        /// <summary>
        /// Gets or sets the page.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        public int Results { get; set; }

        /// <summary>
        /// Gets or sets the seed.
        /// </summary>
        public string Seed { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public string Version { get; set; }
    }
}