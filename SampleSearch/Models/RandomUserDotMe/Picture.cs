// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Picture.cs" company="SampleSearch">
//   Copyright (c) 2016. All Rights Reserved.
// </copyright>
// <summary>
//   The model representing the a Person's Picture returned from the randomuser.me site.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SampleSearch.Models.RandomUserDotMe
{
    /// <summary>
    /// The picture.
    /// </summary>
    public class Picture
    {
        /// <summary>
        /// Gets or sets the large.
        /// </summary>
        public string Large { get; set; }

        /// <summary>
        /// Gets or sets the medium.
        /// </summary>
        public string Medium { get; set; }

        /// <summary>
        /// Gets or sets the thumbnail.
        /// </summary>
        public string Thumbnail { get; set; }
    }
}