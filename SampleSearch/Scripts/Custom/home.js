﻿(function (ss, $) {
    "use strict";

    $(function () {
        ss.common.updatePeopleCount(ss.serverValues.initPeopleCount);

        $("#seedDataButton").on("click", function () {
            const seedAmount = $("#seedAmount").val() > 0
                ? $("#seedAmount").val()
                : 0;
            const confirmDelete = $("#clearData").is(":checked")
                ? "delete all existing people and "
                : "";
            if (confirm("Are you sure you want to " + confirmDelete + "create " + seedAmount + " people?")) {
                ss.seed.seedPeople();
            }
        });

        $("#wipeData").on("click", function () {
            if (confirm("Are you sure you want to delete all people?")) {
                ss.seed.clearPeople();
            }
        });
    });

    $.extend(true, ss, {
        common: {
            updatePeopleCount: function (newAmount) {
                const currentValue = parseInt($("#totalPeople").text().replace(/\D/g, ""));
                newAmount = parseInt(newAmount, 10);
                ss.common.animateNumberValue("#totalPeople", 1000, currentValue, newAmount);
            }
        },
        seed: {
            apiBaseUrl: "/api/person/",
            showLoading: function () {
                $("#dataToolsContainer").find("button").attr("disabled", "disabled").addClass("disabled");
                $("#dataToolsContainer").find("form").hide();
                $("#loadingContainer").fadeIn("fast");
            },
            hideLoading: function () {
                $("#dataToolsContainer").find("button").removeAttr("disabled").removeClass("disabled");
                $("#dataToolsContainer").find("form").fadeIn("fast");
                $("#loadingContainer").hide();
            },
            seedPeople: function () {
                ss.seed.showLoading();
                const seedAmount = $("#seedAmount").val() > 0 ? $("#seedAmount").val() : 0;
                const clearData = $("#clearData").is(":checked");

                $.get(ss.seed.apiBaseUrl + "seed/" + seedAmount + "/" + clearData)
                    .done(function (data) { ss.common.updatePeopleCount(data); })
                    .fail(function () { /* TODO: Show error message to user and/or log error as needed. */ })
                    .always(function () { ss.seed.hideLoading(); });

            },
            clearPeople: function () {
                ss.seed.showLoading();
                $.get(ss.seed.apiBaseUrl + "seed/clear")
                    .done(function (data) { ss.common.updatePeopleCount(data); })
                    .fail(function () { /* TODO: Show error message to user and/or log error as needed. */ })
                    .always(function () { ss.seed.hideLoading(); });
            }
        }
    });
})(window.SampleSearch = window.SampleSearch || {}, window.jQuery);
