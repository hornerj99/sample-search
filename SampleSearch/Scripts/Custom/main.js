﻿(function (hc, $) {
    "use strict";

    $.extend(true, hc, {
        common: {
            animateNumberValue: function (targetElementId, transitionTime, startValue, endValue) {
                $({ someValue: startValue }).animate(
                    { someValue: endValue },
                    {
                        duration: transitionTime,
                        easing: "swing",
                        step: function () {
                            const returnValue = startValue <= endValue
                                ? Math.ceil(this.someValue)
                                : Math.floor(this.someValue);
                            $(targetElementId).text(returnValue.toLocaleString());
                        },
                        complete: function () {
                            $(targetElementId).text(endValue.toLocaleString());
                        }
                    });
            }
        }
    });
})(window.SampleSearch = window.SampleSearch || {}, window.jQuery);
