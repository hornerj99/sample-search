﻿// <copyright file="PersonService.cs" company="SampleSearch">
// Copyright (c) SampleSearch. All rights reserved.
// </copyright>

namespace SampleSearch.Services
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using DAL;
    using Models;
    using Models.RandomUserDotMe;
    using Newtonsoft.Json;

    /// <summary>
    /// The service for Person information.
    /// </summary>
    public class PersonService
    {
        /// <summary>
        /// Totals the people.
        /// </summary>
        /// <returns>Number of people in the database</returns>
        public static int TotalPeople()
        {
            using (var context = new PersonContext())
            {
                return context.People.Count();
            }
        }

        /// <summary>
        /// Search for people.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns>List of matching people</returns>
        public static List<PersonView> FindPeople(string search)
        {
            using (var context = new PersonContext())
            {
                // TODO: Move this to a database procedure for permission checks and add server-side paging.
                // TODO: Implement paging
                return
                    context.People.Where(p => p.FirstName.StartsWith(search) || p.LastName.StartsWith(search))
                        .Select(
                            p =>
                            new PersonView
                            {
                                Gender = p.Gender,
                                FirstName = p.FirstName,
                                LastName = p.LastName,
                                Street = p.Street,
                                City = p.City,
                                State = p.State,
                                PostalCode = p.PostalCode,
                                Nationality = p.Nationality,
                                Cell = p.Cell,
                                Phone = p.Phone,
                                Email = p.Email,
                                Username = p.Username,
                                PictureLarge = p.PictureLarge,
                                PictureMedium = p.PictureMedium,
                                PictureThumb = p.PictureThumbnail,
                                RegistrationDate = p.RegistrationDate,
                                Birthdate = p.Birthdate
                            })
                        .ToList();
            }
        }

        /// <summary>
        /// Seeds the database.
        /// </summary>
        /// <param name="seedUserCountManual">The seed user count manual.</param>
        /// <param name="clearData">The clear data.</param>
        /// <exception cref="System.Exception">
        /// The count for seeding users is missing from the web.config file.
        /// or
        /// The maximum count for seeding users is missing from the web.config file.
        /// or
        /// The URL for seeding users is missing from the web.config file.
        /// or
        /// The source for seeding users is not available
        /// </exception>
        public static void SeedDatabase(int seedUserCountManual = 0, bool clearData = false)
        {
            int seedUserCount;

            if (seedUserCountManual > 0)
            {
                seedUserCount = seedUserCountManual;
            }
            else
            {
                if (!int.TryParse(ConfigurationManager.AppSettings["SeedUserCount"], out seedUserCount))
                {
                    throw new Exception("The count for seeding users is missing from the web.config file.");
                }
            }

            if (!int.TryParse(ConfigurationManager.AppSettings["SeedUserCountLimit"], out int seedUserCountLimit))
            {
                throw new Exception("The maximum count for seeding users is missing from the web.config file.");
            }

            // RandomUser.me seems to have an upper limit of 5000.
            if (seedUserCount > seedUserCountLimit)
            {
                seedUserCount = seedUserCountLimit;
            }

            var urlBase = ConfigurationManager.AppSettings["SeedUserUrl"];
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SeedUserUrl"]))
            {
                throw new Exception("The URL for seeding users is missing from the web.config file.");
            }

            if (clearData)
            {
                ClearDatabase();
            }

            var myRequest = WebRequest.CreateHttp(urlBase + seedUserCount);
            myRequest.Method = "GET";
            myRequest.UserAgent = "WebRequest";
            using (var theResponse = myRequest.GetResponse())
            {
                var dataStream = theResponse.GetResponseStream();
                if (dataStream == null)
                {
                    throw new Exception("The source for seeding users is not available");
                }

                var reader = new StreamReader(dataStream);
                object objResponse = reader.ReadToEnd();
                var peopleToSeed = JsonConvert.DeserializeObject<SeedPerson>(objResponse.ToString());
                dataStream.Close();
                theResponse.Close();
                using (var context = new PersonContext())
                {
                    context.People.AddRange(
                        peopleToSeed.Results.Select(
                            p =>
                            new Person
                            {
                                IdName = p.Id.Name,
                                IdValue = p.Id.Value,
                                Gender = p.Gender,
                                Title = p.Name.Title,
                                FirstName = p.Name.First,
                                LastName = p.Name.Last,
                                Street = p.Location.Street,
                                City = p.Location.City,
                                State = p.Location.State,
                                PostalCode = p.Location.Postcode,
                                Nationality = p.Nat,
                                Cell = p.Cell,
                                Phone = p.Phone,
                                Email = p.Email,
                                Username = p.Login.Username,
                                Password = p.Login.Password,
                                Md5 = p.Login.Md5,
                                Sha1 = p.Login.Sha1,
                                Sha256 = p.Login.Sha256,
                                Salt = p.Login.Salt,
                                PictureLarge = p.Picture.Large,
                                PictureMedium = p.Picture.Medium,
                                PictureThumbnail = p.Picture.Thumbnail,
                                RegistrationDate = p.Registered,
                                Birthdate = p.Dob
                            }));

                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Truncates the database.
        /// </summary>
        public static void ClearDatabase()
        {
            using (var context = new PersonContext())
            {
                // TODO: This is pretty rare to method for the user to truncate all data.  This would typically be wrapped in other permissions and possibly an approval process.
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE Person");
            }
        }
    }
}
