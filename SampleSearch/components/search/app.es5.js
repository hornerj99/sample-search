﻿"use strict";

var _createClass = (function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
})();

var _get = function get(_x, _x2, _x3) {
    var _again = true;_function: while (_again) {
        var object = _x,
            property = _x2,
            receiver = _x3;_again = false;if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
            var parent = Object.getPrototypeOf(object);if (parent === null) {
                return undefined;
            } else {
                _x = parent;_x2 = property;_x3 = receiver;_again = true;desc = parent = undefined;continue _function;
            }
        } else if ("value" in desc) {
            return desc.value;
        } else {
            var getter = desc.get;if (getter === undefined) {
                return undefined;
            }return getter.call(receiver);
        }
    }
};

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var PersonSearch = (function (_React$Component) {
    _inherits(PersonSearch, _React$Component);

    function PersonSearch(props) {
        _classCallCheck(this, PersonSearch);

        _get(Object.getPrototypeOf(PersonSearch.prototype), "constructor", this).call(this, props);
        this.state = {
            items: [],
            userIsTyping: false,
            resultCount: 0,
            viewType: "table"
        };
    }

    _createClass(PersonSearch, [{
        key: "setViewList",
        value: function setViewList() {
            this.changeView("list");
        }
    }, {
        key: "setViewTiles",
        value: function setViewTiles() {
            this.changeView("tile");
        }
    }, {
        key: "setViewTable",
        value: function setViewTable() {
            this.changeView("table");
        }
    }, {
        key: "changeView",
        value: function changeView(viewType) {
            $("#dataContainer").fadeOut("fast");
            $("#loadingContainer").show();
            $("#changeViewContainer").find("button").removeClass("active");

            switch (viewType) {
                case "tile":
                    $("#changeViewTile").addClass("active");
                    break;
                case "table":
                    $("#changeViewTable").addClass("active");
                    break;
                default:
                    $("#changeViewList").addClass("active");
                    break;
            }

            this.setState({ viewType: viewType });

            $("#loadingContainer").hide();
            $("#dataContainer").fadeIn("fast");
        }
    }, {
        key: "searchChanged",
        value: function searchChanged() {
            // TODO: Save unnecessary calls to the database by using a delay and cancelling prior calls as the user presses keys.
            this.getData();
        }
    }, {
        key: "searchDelay",
        value: function searchDelay() {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        }
    }, {
        key: "getData",
        value: function getData() {
            if (this.state.userIsTyping === true) {
                return;
            }

            var self = this;
            var searchString = document.getElementById("searchText").value;
            if (searchString.length > 0) {
                $("#dataContainer").hide();
                $("#loadingContainer").show();
                $.post("/api/person/search/", { "": searchString }).done(function (data) {
                    self.setState({ items: data, resultCount: data.length });
                }).fail(function () {
                    // TODO: Show error message to user and/or log error as needed.
                }).always(function () {
                    $("#loadingContainer").hide();
                    $("#dataContainer").fadeIn("fast");
                });
            } else {
                this.setState({ items: [], resultCount: 0 });
            }
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement("div", null, React.createElement("div", { className: "row" }, React.createElement("div", { className: "col-sm-6" }, React.createElement("h1", null, "People Search"), React.createElement("div", null, "Start typing the first or last name of the person you are looking for.")), React.createElement("div", { className: "col-sm-6" }, React.createElement("div", { id: "searchFormContainer" }, React.createElement("input", { id: "searchText", className: "form-control", type: "text", placeholder: "Start typing a first or last name...", autoFocus: true, onChange: this.searchChanged.bind(this) })))), React.createElement("br", null), React.createElement("br", null), React.createElement("div", { className: "text-center" }), React.createElement("div", { className: "clearfix" }, React.createElement("div", { className: "pull-left" }, React.createElement("h3", null, "Results (", this.state.resultCount, ")")), React.createElement("div", { id: "searchViewsContainer", className: "pull-right text-right" }, React.createElement("div", { id: "changeViewContainer", className: "btn-group" }, React.createElement("button", { id: "changeViewTable", className: "btn btn-xs btn-default active", onClick: this.setViewTable.bind(this), title: "Table View" }, " ", React.createElement("i", { className: "fa fa-bars" })), React.createElement("button", { id: "changeViewList", className: "btn btn-xs btn-default", onClick: this.setViewList.bind(this), title: "Small Tile View" }, React.createElement("i", { className: "fa fa-th" })), React.createElement("button", { id: "changeViewTile", className: "btn btn-xs btn-default", onClick: this.setViewTiles.bind(this), title: "Large Tile View" }, " ", React.createElement("i", { className: "fa fa-th-large" }))))), React.createElement("hr", null), React.createElement(SearchResults, { people: this.state.items, viewType: this.state.viewType }));
        }
    }]);

    return PersonSearch;
})(React.Component);

ReactDOM.render(React.createElement(PersonSearch, null), document.getElementById("personSearchContainer"));

