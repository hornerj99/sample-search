﻿"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResultListItem = (function (_React$Component) {
    _inherits(ResultListItem, _React$Component);

    function ResultListItem() {
        _classCallCheck(this, ResultListItem);

        _get(Object.getPrototypeOf(ResultListItem.prototype), "constructor", this).apply(this, arguments);
    }

    _createClass(ResultListItem, [{
        key: "render",
        value: function render() {
            var person = this.props.person;
            var index = this.props.index;
            return React.createElement(
                "div",
                { key: index, className: "resultsContainerList" },
                React.createElement(
                    "div",
                    { className: "col-lg-6 col-sm-12" },
                    React.createElement(
                        "div",
                        { className: "media resultListItem text-left" },
                        React.createElement(
                            "div",
                            { className: "media-left" },
                            React.createElement(
                                "a",
                                { href: "#" },
                                React.createElement("img", { src: person.pictureMedium, className: "media-object img-circle", alt: "Picture" })
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "media-body" },
                            React.createElement(
                                "div",
                                { className: "media-heading" },
                                React.createElement(
                                    "span",
                                    { className: "firstLastName" },
                                    person.firstName,
                                    " ",
                                    person.lastName
                                ),
                                React.createElement(
                                    "span",
                                    { className: "muted" },
                                    "(",
                                    person.username,
                                    ")"
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "addressContainer", title: person.street + " " + person.city + ", " + person.state + " " + person.postalCode + " " + person.nationality },
                                person.street,
                                " ",
                                person.city,
                                ", ",
                                person.state,
                                " ",
                                person.postalCode,
                                React.createElement("img", { src: "http://www.geognos.com/api/en/countries/flag/" + person.nationality + ".png", className: "flag", height: "14", alt: person.nat })
                            ),
                            React.createElement(
                                "div",
                                null,
                                person.phone
                            )
                        )
                    )
                )
            );
        }
    }]);

    return ResultListItem;
})(React.Component);

