﻿class ResultList extends React.Component {
    render() {
        return (
            <div className="resultsContainerList row">
                {this.props.people.map(function (person, index) {
                    return <ResultListItem person={person} index={index} key={index} />;
                })}
            </div>
        );
    }
}
