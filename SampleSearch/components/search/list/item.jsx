﻿class ResultListItem extends React.Component {
    render() {
        const person = this.props.person;
        const index = this.props.index;
        return (
            <div key={index} className="col-lg-6 col-sm-12">
                <div className="media resultListItem">
                    <a href="#" className="mr-3"><img className="align-self-start rounded-circle" src={person.pictureMedium} alt="Profile Picture" /></a>

                    <div className="media-body">
                        <div className="media-heading">
                            <span className="firstLastName">{person.firstName} {person.lastName}</span>
                            <span className="muted">({person.username})</span>
                        </div>
                        <div className="addressContainer text-truncate" title={person.street + " " + person.city + ", " + person.state + " " + person.postalCode + " " + person.nationality}>
                            {person.street} {person.city}, {person.state} {person.postalCode}
                            <img src={"http://www.geognos.com/api/en/countries/flag/" + person.nationality + ".png"} className="flag" height="14" alt={person.nat} />
                        </div>
                        <div>{person.phone}</div>
                    </div>
                </div>

            </div>
        );
    }
}
