﻿"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResultTiles = (function (_React$Component) {
    _inherits(ResultTiles, _React$Component);

    function ResultTiles() {
        _classCallCheck(this, ResultTiles);

        _get(Object.getPrototypeOf(ResultTiles.prototype), "constructor", this).apply(this, arguments);
    }

    _createClass(ResultTiles, [{
        key: "getAge",
        value: function getAge(dateString) {
            // NOTE: This is not completely accurate since it does not account for leap years.
            // Also, this age should be calculated on the server, rather than here at the client.
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
                age--;
            }
            return age;
        }
    }, {
        key: "getRandomInterests",
        value: function getRandomInterests() {
            // NOTE: One of the requirements mentioned showing Interests, which were not part of the imported data.  For the demo, I just added these on the UI.
            // Normally, these would be in an "Interest" table in the database along with a PersonInterest table to tie the person to 0 to many interests.
            var interests = ["Volleyball", "Chess", "Skiing", "Basket Weaving", "Bowling", "Pickle Ball", "Horseshoes", "Cooking", "Photgraphy", "Spelunking"];
            var interest, x;
            var returnInterests = [];
            var count = Math.floor(Math.random() * 3 + 1);
            for (x = 0; x <= count; x++) {
                interest = interests[Math.floor(Math.random() * interests.length)];
                if (returnInterests.indexOf(interest) < 0) {
                    returnInterests.push(interest);
                }
            }
            return returnInterests.join(", ");
        }
    }, {
        key: "render",
        value: function render() {
            var person = this.props.person;
            var index = this.props.index;
            var dob = person.birthdate;
            var dobFormatted = new Date(dob).toLocaleDateString();
            var age = this.getAge(dob);
            return React.createElement(
                "div",
                { key: index, className: "resultsContainerTile" },
                React.createElement(
                    "div",
                    { className: "tile-container col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center" },
                    React.createElement(
                        "div",
                        { className: "col-md-12 col-xs-12 tile-header-outer text-center" },
                        React.createElement(
                            "div",
                            { className: "tile-header" },
                            React.createElement(
                                "div",
                                { className: "name-container" },
                                person.firstName,
                                " ",
                                person.lastName
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "outer" },
                            React.createElement("img", { src: person.pictureLarge, className: "image-circle", alt: "Picture" })
                        ),
                        React.createElement(
                            "h4",
                            { className: "emailContainer", title: person.email },
                            React.createElement(
                                "a",
                                { href: "mailto:noam.moulin@somplace.com" },
                                person.email
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-md-6 col-xs-6 follow line text-center" },
                        React.createElement(
                            "h4",
                            null,
                            person.cell,
                            React.createElement("br", null),
                            " ",
                            React.createElement(
                                "span",
                                { className: "phoneLabel" },
                                "Mobile"
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-md-6 col-xs-6 follow line text-center" },
                        React.createElement(
                            "h4",
                            null,
                            person.phone,
                            React.createElement("br", null),
                            " ",
                            React.createElement(
                                "span",
                                { className: "phoneLabel" },
                                "Home"
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-md-12 col-xs-12 additional-information text-left" },
                        React.createElement(
                            "div",
                            { className: "control" },
                            React.createElement(
                                "div",
                                { className: "label" },
                                "Location ",
                                React.createElement("img", { src: "http://www.geognos.com/api/en/countries/flag/" + person.nationality + ".png", className: "flag", height: "14", alt: person.nationality + " Flag" })
                            ),
                            React.createElement(
                                "div",
                                { className: "form-control ellipsisText" },
                                React.createElement(
                                    "address",
                                    { className: "ellipsisText", title: person.street + " " + person.city + ", " + person.state + " " + person.postalCode + " " + person.nationality },
                                    person.street,
                                    " ",
                                    person.city,
                                    ", ",
                                    person.state,
                                    " ",
                                    person.postalCode,
                                    " ",
                                    person.nationality
                                )
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "control" },
                            React.createElement(
                                "div",
                                { className: "label" },
                                "Date of Birth"
                            ),
                            React.createElement(
                                "div",
                                { className: "form-control" },
                                dobFormatted,
                                " (",
                                age,
                                " years old)"
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "control" },
                            React.createElement(
                                "div",
                                { className: "label" },
                                "Interests"
                            ),
                            React.createElement(
                                "div",
                                { className: "form-control ellipsisText" },
                                this.getRandomInterests()
                            )
                        )
                    )
                )
            );
        }
    }]);

    return ResultTiles;
})(React.Component);

