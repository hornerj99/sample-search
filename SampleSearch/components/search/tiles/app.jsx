﻿class ResultTiles extends React.Component {
    getAge(dateString) {
        // NOTE: This is not completely accurate since it does not account for leap years.
        // Also, this age should be calculated on the server, rather than here at the client.
        const today = new Date();
        const birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
            age--;
        }
        return age;
    }
    getRandomInterests() {
        // NOTE: Normally, these would be in an "Interest" table in the database along with a PersonInterest table to tie the person to 0 to many interests.
        const interests = ["Volleyball", "Chess", "Skiing", "Basket Weaving", "Bowling", "Pickle Ball", "Horseshoes", "Cooking", "Photgraphy", "Spelunking"];
        let interest;
        let x;
        const returnInterests = [];
        const count = Math.floor(Math.random() * 3 + 1);
        for (x = 0; x <= count; x++) {
            interest = interests[Math.floor(Math.random() * interests.length)];
            if (returnInterests.indexOf(interest) < 0) {
                returnInterests.push(interest);
            }
        }
        return returnInterests.join(", ");
    }
    render() {
        const person = this.props.person;
        const index = this.props.index;
        const dob = person.birthdate;
        const dobFormatted = (new Date(dob)).toLocaleDateString();
        const age = this.getAge(dob);
        return (
            <div key={index} className="tile-container col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center resultsContainerTile">
                <div className="tile-header-outer">
                    <div className="tile-header">
                        <div className="name-container">{person.firstName} {person.lastName}</div>
                    </div>
                    <div className="outer"><img src={person.pictureLarge} className="image-circle" alt="Picture" /></div>
                    <h4 className="emailContainer" title={person.email}><a href="mailto:noam.moulin@somplace.com">{person.email}</a></h4>
                </div>
                <div className="follow">
                    <h4 className="text-nowrap">{person.cell}<br /> <span className="phoneLabel">Mobile</span></h4>
                </div>
                <div className="follow">
                    <h4 className="text-nowrap">{person.phone}<br /> <span className="phoneLabel">Home</span></h4>
                </div>
                <div className="additional-information text-left">
                    <div className="control">
                        <div className="label">Location <img src={"http://www.geognos.com/api/en/countries/flag/" + person.nationality + ".png"} className="flag" height="14" alt={person.nationality + " Flag"} /></div>
                        <div className="form-control ellipsisText">
                            <address className="ellipsisText" title={person.street + " " + person.city + ", " + person.state + " " + person.postalCode + " " + person.nationality}>
                                {person.street} {person.city}, {person.state} {person.postalCode} {person.nationality}
                            </address>
                        </div>
                    </div>
                    <div className="control">
                        <div className="label">Date of Birth</div>
                        <div className="form-control">{dobFormatted} ({age}&nbsp;years&nbsp;old)</div>
                    </div>
                    <div className="control">
                        <div className="label">Interests</div>
                        <div className="form-control text-truncate" title={this.getRandomInterests()}>{this.getRandomInterests()}</div>
                    </div>
                </div>
            </div>
        );
    }
}
