﻿"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResultTableRow = (function (_React$Component) {
    _inherits(ResultTableRow, _React$Component);

    function ResultTableRow(props) {
        _classCallCheck(this, ResultTableRow);

        _get(Object.getPrototypeOf(ResultTableRow.prototype), "constructor", this).call(this, props);
    }

    _createClass(ResultTableRow, [{
        key: "formatDate",
        value: function formatDate(rawDate) {
            return new Date(rawDate).toLocaleDateString();
        }
    }, {
        key: "render",
        value: function render() {
            var person = this.props.person;
            return React.createElement(
                "tr",
                { key: this.props.index },
                React.createElement(
                    "td",
                    null,
                    React.createElement("img", { src: person.pictureThumb, alt: person.pictureThumb, className: "img-responsive" })
                ),
                React.createElement(
                    "td",
                    null,
                    React.createElement(
                        "a",
                        { href: "mailto:" + person.email },
                        person.firstName,
                        " ",
                        person.lastName
                    )
                ),
                React.createElement(
                    "td",
                    null,
                    person.street
                ),
                React.createElement(
                    "td",
                    null,
                    person.city
                ),
                React.createElement(
                    "td",
                    null,
                    person.state
                ),
                React.createElement(
                    "td",
                    null,
                    person.postalCode
                ),
                React.createElement(
                    "td",
                    null,
                    person.nationality
                ),
                React.createElement(
                    "td",
                    null,
                    person.username
                ),
                React.createElement(
                    "td",
                    null,
                    this.formatDate(person.birthdate)
                ),
                React.createElement(
                    "td",
                    null,
                    person.phone
                )
            );
        }
    }]);

    return ResultTableRow;
})(React.Component);

