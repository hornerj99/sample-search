﻿"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResultTable = (function (_React$Component) {
    _inherits(ResultTable, _React$Component);

    function ResultTable() {
        _classCallCheck(this, ResultTable);

        _get(Object.getPrototypeOf(ResultTable.prototype), "constructor", this).apply(this, arguments);
    }

    _createClass(ResultTable, [{
        key: "render",
        value: function render() {
            var people = this.props.people;
            return React.createElement(
                "div",
                { className: "table-responsive" },
                React.createElement(
                    "table",
                    { className: "datatable table table-hover table-condensed table-striped dt-responsive nowrap" },
                    React.createElement(
                        "thead",
                        null,
                        React.createElement(
                            "tr",
                            null,
                            React.createElement(
                                "th",
                                null,
                                "Picture"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Name"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Street"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "City"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "State"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Postal Code"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Nationality"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Username"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Birthdate"
                            ),
                            React.createElement(
                                "th",
                                null,
                                "Phone"
                            )
                        )
                    ),
                    React.createElement(
                        "tbody",
                        null,
                        people.map(function (person, index) {
                            return React.createElement(ResultTableRow, { person: person, index: index, key: index });
                        })
                    )
                )
            );
        }
    }]);

    return ResultTable;
})(React.Component);

