﻿class ResultTableRow extends React.Component {
    constructor(props) {
        super(props);
    }
    formatDate(rawDate) {
        return new Date(rawDate).toLocaleDateString();
    }
    render() {
        const person = this.props.person;
        return (
            <tr key={this.props.index}>
                <td><img src={person.pictureThumb} alt={person.pictureThumb} className="img-responsive" /></td>
                <td><a href={"mailto:" + person.email}>{person.firstName} {person.lastName}</a></td>
                <td>{person.street}</td>
                <td>{person.city}</td>
                <td>{person.state}</td>
                <td>{person.postalCode}</td>
                <td>{person.nationality}</td>
                <td>{person.username}</td>
                <td>{this.formatDate(person.birthdate)}</td>
                <td>{person.phone}</td>
            </tr>
        );
    }
}
