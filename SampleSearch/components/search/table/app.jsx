﻿class ResultTable extends React.Component {
    render() {
        const people = this.props.people;
        return (
            <div className="table-responsive">
                <table className="datatable table table-sm table-hover table-condensed table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Picture</th>
                            <th>Name</th>
                            <th>Street</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Postal Code</th>
                            <th>Nationality</th>
                            <th>Username</th>
                            <th>Birthdate</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {people.map(function (person, index) {
                            return (<ResultTableRow person={person} index={index} key={index} />);
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
