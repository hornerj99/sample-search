﻿class PersonSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            userIsTyping: false,
            resultCount: 0,
            viewType: "table"
        };
    }
    setViewList() {
        this.changeView("list");
    }
    setViewTiles() {
        this.changeView("tile");
    }
    setViewTable() {
        this.changeView("table");
    }
    changeView(viewType) {
        $("#dataContainer").fadeOut("fast");
        $("#loadingContainer").show();
        $("#changeViewContainer").find("button").removeClass("active");

        switch (viewType) {
            case "tile":
                $("#changeViewTile").addClass("active");
                break;
            case "table":
                $("#changeViewTable").addClass("active");
                break;
            default:
                $("#changeViewList").addClass("active");
                break;
        }

        this.setState({ viewType: viewType });

        $("#loadingContainer").hide();
        $("#dataContainer").fadeIn("fast");
    }
    searchChanged() {
        // TODO: Save unnecessary calls to the database by using a delay and cancelling prior calls as the user presses keys.
        this.getData();
    }
    searchDelay() {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    }
    getData() {
        if (this.state.userIsTyping === true) {
            return;
        }

        var self = this;
        const searchString = document.getElementById("searchText").value;
        if (searchString.length > 0) {
            $("#dataContainer").hide();
            $("#loadingContainer").show();
            $.post("/api/person/search/", { "": searchString })
                .done(function (data) {
                    self.setState({ items: data, resultCount: data.length });
                })
                .fail(function () {
                    // TODO: Show error message to user and/or log error as needed.
                })
                .always(function () {
                    $("#loadingContainer").hide();
                    $("#dataContainer").fadeIn("fast");
                });
        } else {
            this.setState({ items: [], resultCount: 0 });
        }

    }
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-sm-6">
                        <h1>Search</h1>
                        <div>Start typing the first or last name and results will appear below.</div>
                    </div>
                    <div className="col-sm-6">
                        <div id="searchFormContainer">
                            <input id="searchText" className="form-control" type="text" placeholder="Start typing first or last name..." autoFocus={true} onChange={this.searchChanged.bind(this)} />
                        </div>
                    </div>
                </div>

                <br /><br />
                <div className="text-center">
                </div>
                <div className="clearfix">
                    <div className="pull-left">
                        <h3>Results ({this.state.resultCount})</h3>
                    </div>
                    <div id="searchViewsContainer" className="pull-right text-right">
                        <div id="changeViewContainer" className="btn-group">
                            <button id="changeViewTable" className="btn btn-xs btn-light active" onClick={this.setViewTable.bind(this)} title="Table View"> <i className="fa fa-bars"></i></button>
                            <button id="changeViewList" className="btn btn-xs btn-light" onClick={this.setViewList.bind(this)} title="Small Tile View"><i className="fa fa-th"></i></button>
                            <button id="changeViewTile" className="btn btn-xs btn-light" onClick={this.setViewTiles.bind(this)} title="Large Tile View"> <i className="fa fa-th-large"></i></button>
                        </div>
                    </div>
                </div>
                <hr />
                <SearchResults people={this.state.items} viewType={this.state.viewType} />
            </div>
        );
    }
}

ReactDOM.render(<PersonSearch />, document.getElementById("personSearchContainer"));
