﻿class SearchResults extends React.Component {
    render() {
        const people = this.props.people;
        if (people.length === 0) {
            return (<div>No matches found.</div>);
        } else {
            switch (this.props.viewType) {
                case "tile":
                    return (
                        <div id="dataContainer" className="row">
                            {people.map(function (person, index) {
                                return <ResultTiles person={person} index={index} key={index} />;
                            })}
                        </div>
                    );
                case "table":
                    return (<ResultTable people={people} />);
                default:
                    return (<ResultList people={people} />);
            }
        }
    }
}
